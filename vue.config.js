const { defineConfig } = require('@vue/cli-service')
module.exports = {
  lintOnSave: false,
  devServer: {
    proxy: {
    "/api": {
    target: "https://dahua0822-api.herokuapp.com/cart",
    ws: true,
    changeOrigin: true,
    pathRewrite: {
    "^/api": ""
    }
    }
    }
    },
    
};
